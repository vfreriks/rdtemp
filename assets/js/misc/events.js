/**
 * @section : Unobtrusive JavaScript events and function calls, triggered on DOMContentLoaded.
 * @project : PROJECT_NAAM
 * @author  : Naam Developer <developer@e-sites.nl>
 */

(function (window, document, $, app) {

	'use strict';

	// Main cachebuster for lazyloaded scripts
	$script.urlArgs('bust=' + app.cachebuster);

	/**
	 * Main init function that kickstarts all site logic when the DOM is loaded
	 * Make sure all event handlers are placed within this function
	 *
	 * @private
	 */

	function _init() {
        /**
		 * Detect whether delayed gif is in view & switch src if true
		 * After that; detect this on scroll
         */
		app.handleGifs();
        $(window).scroll(function(){
            app.handleGifs();
		});

        // Mimic autofocus for IE<10
		$('[autofocus]').autofocus();

		// Parsley based form validation
		app.util.initFormValidation('.js-validate-form');

		// Fancy scrollTo logic
		$('.js-scrollto').on('click', app.util.scrollTo);

		// Custom event tracking
		// Tracks external links and downloads by default
		// Use vestigo.init().debug(); to see all logs
		vestigo.init();

		var ieversion = app.msieversion();

		if(ieversion){
            PointerEventsPolyfill.initialize({
            	selector: '.contact *, .header-wrapper *, .hero *',
				mouseEvents: ['click']
			});
		}

        // Close sidebar menu
        $('.js-toggle-menu').on('click', function() {
            $('.menu-sidebar').toggleClass('hidden-menu');
            $('body').toggleClass('menu-opened');
        });

        $('html body.menu-opened').click(function(e){
        	$('.menu-sidebar').addClass('hidden-menu');
		});


		// Initialize Lightbox
		$('.video-play-btn').magnificPopup({
			type: 'iframe'
		});

		$('.js-open-popup-link').magnificPopup({
            type: 'inline'
        });

        var video = $('#html5video');
		$('.js-open-video').magnificPopup({
            type: 'inline',
			callbacks: {
            	open: function(){
					video.trigger('play')
				}
			}
        });


		// hide header and menu text on scroll
		var mywindow = $(window);
		var headerHeight = $('.header-wrapper').height() - 20;
		var mypos = headerHeight;
		var up = false;
		var newscroll;
		mywindow.scroll(function () {
			newscroll = mywindow.scrollTop();
			headerHeight = $('.header-wrapper').height() - 20;

			if (newscroll > headerHeight && !up) {
				$('.header-wrapper').addClass('fade-out');
				$('.menu-btn').addClass('fade-out');
				up = !up;
			} else if(newscroll < headerHeight && up) {
				$('.header-wrapper').removeClass('fade-out');
				$('.menu-btn').removeClass('fade-out');
				up = !up;
			}
			mypos = newscroll;
		});

		// General event delegation
		// Use this $(document) reference to chain other delegated events
		$(document)
			// Handle external links (the old way)
			.on('click', 'a[rel="external"]', app.util.setExtLinks)

			// Focus search box when pressing '/'
			.on('keyup', app.util.focusSearchBox)

			// Handle location switch
			.on('click', '.location__link', app.locationSwitch)


	}

	// Initialize
	$(document)
		.ready(_init)
		.ajaxError(app.util.processXhrError);


}(window, document, jQuery, app));