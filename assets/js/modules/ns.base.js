/**
 * Site-specific methods that do not belong in functions.js
 * and can't be grouped in a separated module
 *
 * @author Naam Developer <developer@e-sites.nl>
 */

/* global ns, jQuery */
(function (window, $) {

	'use strict';

	var win = window,
		doc = win.document,

		// Cached DOM elements
		$win = $(win),
		$html = $('html'),

		// Private module variables
		_foo = 10;

	/**
	 * Extend namespace with site-specific methods
	 * These methods can be called as follows: `ns.methodName()`
	 */
	$.extend(ns, {

		/**
		 * Handles newletter signup via AJAX
		 *
		 * @author Boye Oomens <boye@e-sites.nl>
		 * @param  {Object} e jQuery event object
		 */
		handleNewsletterSignup: function (e) {
			var $self = $(this),
				btn = Ladda.create( $(this).find('.ladda-button')[0] );

			e.preventDefault();

			if ( $self.parsley().isValid() ) {

				btn.start();

				$.post('/xhr/newsletter/signup.json', $self.serialize())
				.then(function () {
					setTimeout(function () {
						$self.html('<p class="success-container animated fadeIn">' + $self.data('signupSuccess') + '</p>');
						btn.stop();
					}, 1000);
				}).fail(function () {
					$self.html('<p class="parsley-error-container animated fadeIn">' + $self.data('signupFail') + '</p>');
					btn.stop();
				});
			}
		}

	});

})(this, jQuery);