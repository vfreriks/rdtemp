// /**
//  * An implementation of the Facebook Page Plugin where it's
//  * only getting loaded if it's within the users viewport.
//  *
//  * @author  Boye Oomens <boomens@e-sites.net>
//  * @version 0.1.0
//  * @see https://developers.facebook.com/docs/plugins/page-plugin
//  */
//
// /**
//  * Distillery namespace
//  *
//  * @type {Object}
//  */
// var dst = dst || {};
//
// (function (window, document, $, dst) {
//
// 	'use strict';
//
// 	var pagePlugin;
//
// 	/**
// 	 * Facebook app ID
// 	 *
// 	 * @see https://developers.facebook.com/apps
// 	 * @type {String}
// 	 */
// 	app.fbAppID = '474743405962348';
//
// 	/**
// 	 * Locales, needed for FB SDK
// 	 *
// 	 * @type {Object}
// 	 */
// 	app.locale = {
// 		'nl': 'nl_NL',
// 		'en': 'en_US'
// 	};
//
// 	// Copy the following methods to the base module
// 	$.extend(dst, {
//
// 		/**
// 		 * Async FB init
// 		 *
// 		 * @author John van Hulsen <jvhulsen@e-sites.nl>
// 		 */
// 		fbInit: function () {
// 			FB.init({
// 				appId: app.fbAppID,
// 				xfbml: true,
// 				version: 'v2.3'
// 			});
// 		},
//
// 		/**
// 		 * Injects FB's JavaScript SDK
// 		 * Passes along the active locale as well
// 		 *
// 		 * @author John van Hulsen <jvhulsen@e-sites.nl>
// 		 */
// 		loadFbSDK: function () {
// 			var $html = $html || $('html');
//
// 			window.fbAsyncInit = dst.fbInit;
//
// 			(function (d, s, id) {
// 				var js, fjs = d.getElementsByTagName(s)[0];
// 				if (d.getElementById(id)) {
// 					return;
// 				}
// 				js = d.createElement(s);
// 				js.id = id;
// 				js.src = 'https://connect.facebook.net/' + (app.locale[ $html.attr('lang') ] || 'en_US') + '/sdk.js';
// 				fjs.parentNode.insertBefore(js, fjs);
// 			}(document, 'script', 'facebook-jssdk'));
// 		},
//
// 		/**
// 		 * Initializes FB Page Plugin logic by using the perfscroll plugin
// 		 * Also, we call the fn once manually to make sure that it will show when there is nothing to scroll
// 		 *
// 		 * @author Boye Oomens <boye@e-sites.nl>
// 		 */
// 		initPagePlugin: function () {
// 			pagePlugin = $('#page-plugin')[0];
// 			$(window).perfscroll(dst.showPagePlugin, 100);
// 			dst.showPagePlugin();
// 		},
//
// 		/**
// 		 * Shows FB Page Plugin when user scrolls to the footer
// 		 *
// 		 * @author Boye Oomens <boye@e-sites.nl>
// 		 */
// 		showPagePlugin: function () {
// 			/* global FB, verge */
// 			if ( verge.inViewport(pagePlugin) && !window.FB ) {
// 				dst.loadFbSDK();
// 			}
// 		}
// 	});
//
// 	$(dst.initPagePlugin);
//
// }(this, this.document, jQuery, dst));