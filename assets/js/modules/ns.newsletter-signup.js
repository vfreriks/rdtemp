/**
 * Newsletter signup component
 *
 * Dynamic newsletter signup based on Parsley, JSend2 and Ladda.
 *
 * @author  Boye Oomens <boomens@e-sites.nl>
 * @version 0.1.0
 */

/**
 * Distillery namespace
 *
 * @type {Object}
 */
var dst = dst || {};

/* global Ladda, JSend */
(function (window, document, $, dst) {

	'use strict';

	/**
	 * Local namespace
	 *
	 * @type {Object}
	 */
	var ns = {};

	/**
	 * API endpoint
	 *
	 * @type {String}
	 */
	ns.entrypoint = '/xhr/newsletter/signup.json';

	/**
	 * Reference to the actual form element
	 *
	 * @type {HTMLElement}
	 */
	ns.$form = null;

	/**
	 * Loader reference (i.e. Ladda button)
	 * @type {Object}
	 */
	ns.loader = null;

	/**
	 * Template string with the success message
	 *
	 * @type {String}
	 */
	ns.successTpl = $('#ns-success-tpl').html();

	/**
	 * Template string with the fail message
	 *
	 * @type {String}
	 */
	ns.failTpl = $('#ns-fail-tpl').html();

	/**
	 * Delay in milliseconds
	 * Used to show the indicator a bit longer
	 *
	 * @type {Number}
	 */
	ns.delay = 1000;

	/**
	 * Handles newletter signup via AJAX
	 *
	 * @author Boye Oomens <boye@e-sites.nl>
	 * @param  {Object} e jQuery event object
	 * @private
	 */
	function _handleNewsletterSignup(e) {
		ns.$form = $(this);

		e.preventDefault();

		// Are we dealing with a valid form?
		if ( ns.$form.parsley() && !ns.$form.parsley().isValid() ) {
			return;
		}

		// Create loader reference and kickstart it
		ns.loader = Ladda.create( ns.$form.find('.ladda-button')[0] );
		ns.loader.start();

		setTimeout(_sendData, ns.delay);
	}

	/**
	 * Actual sends the data to the server via JSend
	 *
	 * @author Boye Oomens <boye@e-sites.nl>
	 */
	function _sendData() {
		JSend({
			url: ns.entrypoint,
			data: ns.$form.serialize(),
			success: _handleResponse,
			fail: _handleResponse,
			error: _handleResponse,
			complete: ns.loader.stop
		});
	}

	/**
	 * Handles both the successful as well as a failed response
	 *
	 * @param  {Object} res JSON response object
	 * @private
	 */
	function _handleResponse(res) {
		if ( typeof res === 'string' ) {
			return ns.$form.html(ns.failTpl);
		}
		ns.$form.html(ns.successTpl);
	}

	$('.ns-form-wrapper').on('submit', _handleNewsletterSignup);

}(this, this.document, jQuery, dst));